import { Todo } from './todo';

describe('Todo', () => {

  it('should create an instance', () => {
    expect(new Todo()).toBeTruthy();
  });

  it('should recieve input values in constructor', () => {
    let task = new Todo({
      name: 'Need more cowbell',
      completed: false
    });
    expect(task.name).toEqual('Need more cowbell');
    expect(task.completed).toEqual(false);
  });
});
