import {TestBed, async, inject} from '@angular/core/testing';
import {Todo} from './todo';
import {TodoDataService} from './todo-data.service';

describe('TodoDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoDataService]
    });
  });

  it('should be created', inject([TodoDataService], (service: TodoDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getAllItems()', () => {

    it('should intially return empty array',
    inject([TodoDataService], (service: TodoDataService) => {
      expect(service.getAllItems()).toEqual([]);
    }));

    it('should return all items',
      inject([TodoDataService], (service: TodoDataService) => {
        let todo1 = new Todo({name: 'Test 1', completed: true});
        let todo2 = new Todo({name: 'Test 2', completed: false});

        service.addItem(todo1);
        service.addItem(todo2);

        expect(service.getAllItems()).toEqual([todo1, todo2]);

      }));
  });



  describe('#save(todo)', () => {

    it('should save an incrementing id',
      inject([TodoDataService], (service: TodoDataService) => {

        let todo1 = new Todo({name: 'Test 1', completed: true});
        let todo2 = new Todo({name: 'Test 2', completed: false});

        service.addItem(todo1);
        service.addItem(todo2);

        expect(service.getItemById(1)).toEqual(todo1);
        expect(service.getItemById(2)).toEqual(todo2);
    }));
  });



  describe('#deleteItemById(id)', () => {

    it('should delete the task with corresponding id', inject([TodoDataService], (service: TodoDataService) => {

      let todo1 = new Todo({name: 'Test 1', completed: true});
      let todo2 = new Todo({name: 'Test 2', completed: false});

      service.addItem(todo1);
      service.addItem(todo2);

      expect(service.getAllItems()).toEqual([todo1, todo2]);
      service.deleteItemById(1);
      expect(service.getAllItems()).toEqual([todo2]);
      service.deleteItemById(2);
      expect(service.getAllItems()).toEqual([]);
    }));

    it('should do nothing if task is not found', inject([TodoDataService], (service: TodoDataService) => {

      let todo1 = new Todo({name: 'Test 1', completed: true});
      let todo2 = new Todo({name: 'Test 2', completed: false});

      service.addItem(todo1);
      service.addItem(todo2);

      expect(service.getAllItems()).toEqual([todo1, todo2]);
      service.deleteItemById(3);
      expect(service.getAllItems()).toEqual([todo1, todo2]);
    }));

  });



  describe('#updateItemById(id, values)', () => {

      it('should return task and updated data', inject([TodoDataService], (service: TodoDataService) => {

        let todo = new Todo({title: 'Hello 1', complete: false});
        service.addItem(todo);
        let updatedItem = service.updateItemById(1, {
          name: 'new title'
        });

        expect(updatedItem.name).toEqual('new title');

      }));

      it('should return null if task is not found', inject([TodoDataService], (service: TodoDataService) => {

        let todo = new Todo({name: 'Hello 1', complete: false});
        service.addItem(todo);
        let updatedItem = service.updateItemById(2, {
          name: 'new title'
        });
        expect(updatedItem).toEqual(null);

      }));

    });



    describe('#toggleItemComplete(todo)', () => {

      it('should return the updated todo with inverse complete status', inject([TodoDataService], (service: TodoDataService) => {

        let todo = new Todo({name: 'Hello 1', complete: false});
        service.addItem(todo);
        let updatedTodo = service.toggleItemCompleted(todo);

        expect(updatedTodo.completed).toEqual(true);
        service.toggleItemCompleted(todo);
        expect(updatedTodo.completed).toEqual(false);

      }));

    });

  });



