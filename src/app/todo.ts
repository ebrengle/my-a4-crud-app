export class Todo {
  id: number;
  name: string = '';
  completed: boolean = false;

  constructor(values: Object = {}){
    Object.assign(this, values);
  }
}

let task = new Todo({
    name: 'Build basic A4 CRUD app',
    completed: false
  });
