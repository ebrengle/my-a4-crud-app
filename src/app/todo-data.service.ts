import { Injectable } from '@angular/core';
import {Todo} from './todo';

@Injectable()
export class TodoDataService {

  lastId: number = 0;

  todos: Todo[] = [];

  constructor() { }


  //Create Item
  addItem(todo: Todo): TodoDataService {
    if (!todo.id) {
      todo.id = ++this.lastId;
    }
    this.todos.push(todo);
    return this;
  }

  //Read Items/Item
  getAllItems(): Todo[] {
    return this.todos;
  }

  getItemById(id: number): Todo {
    return this.todos
      .filter(todo => todo.id === id)
      .pop();
  }

  //Update Item
  updateItemById(id: number, values: Object = {}): Todo {
    let todo = this.getItemById(id);
    if (!todo) {
      return null;
    }

    Object.assign(todo, values);
    return todo;
  }

  //Delete
  deleteItemById(id: number): TodoDataService {
    this.todos = this.todos
      .filter(todo => todo.id !== id);
      return this;
  }

  //Mark as complete
  toggleItemCompleted(todo : Todo){
    let updatedTodo = this.updateItemById(todo.id, {
      completed: !todo.completed
    });
    return updatedTodo;
  }

}
